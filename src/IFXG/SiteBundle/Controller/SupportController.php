<?php

namespace IFXG\SiteBundle\Controller;

use IFXG\SiteBundle\Entity\Contact;
use IFXG\SiteBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SupportController extends Controller
{

	public function indexAction(Request $request)
    {

        //Create a new contact entity instance
        $contact = new Contact();
        $form = $this->createForm(new ContactType(), $contact);

        if ($request->isMethod('POST')) {

            //Bind the posted data to the form
            $form->bind($request);

            if ($form->isValid()) {

                //Get the entity manager and persist the contact
                $em = $this->getDoctrine()->getManager();
                $em->persist($contact);
                $em->flush();

                //Redirect the user and add a thank you message
                $message = \Swift_Message::newInstance()
                    ->setSubject('IFXG - ' . $form->get('subject')->getData())
	                ->setTo($this->container->getParameter('send_emails_to'))
	                ->setFrom($this->container->getParameter('swiftmailer.sender_address'))
                    ->setBody(
                        $this->renderView(
                            'SiteBundle:Mail:contact.html.twig',
                            array(
                                'ip' => $request->getClientIp(),
                                'name' => $form->get('name')->getData(),
                                'message' => $form->get('message')->getData()
                            )
                        )
                    );

                $this->get('mailer')->send($message);

                $request->getSession()->getFlashBag()->add('success', 'Your email has been sent! Thanks!');

                return $this->redirect($this->generateUrl('_support'));
            }
        }

        return $this->render('SiteBundle:Default:support.html.twig', array(
            'form' => $form->createView(),
        ));

    }

	public function frequentlyAskedQuestionsAction()
    {
        return $this->render('SiteBundle:Default:frequently-asked-questions.html.twig');
    }

	public function formsAgreementsAction()
    {
        return $this->render('SiteBundle:Default:forms-agreements.html.twig');
    }

}
