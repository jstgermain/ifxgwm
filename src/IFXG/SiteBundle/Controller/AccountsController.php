<?php

namespace IFXG\SiteBundle\Controller;

use IFXG\SiteBundle\Entity\CreateAccount;
use IFXG\SiteBundle\Form\CreateAccountType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountsController extends Controller
{

	public function indexAction()
	{
		return $this->render('SiteBundle:Default:login.html.twig');
	}

	public function createAction(Request $request)
	{

		//Create a new contact entity instance
		$createAccount = new CreateAccount();
		$form = $this->createForm(new CreateAccountType(), $createAccount);

		if ($request->isMethod('POST')) {

			//Bind the posted data to the form
			$form->bind($request);

			if ($form->isValid()) {

				//var_dump($form); die;

				//Get the entity manager and persist the contact
				$em = $this->getDoctrine()->getManager();
				$em->persist($createAccount);
				$em->flush();

				//Redirect the user and add a thank you message
				$message = \Swift_Message::newInstance()
					->setSubject('IFXG - Account Creation Request')
					->setTo($this->container->getParameter('send_emails_to'))
					->setFrom(array($this->container->getParameter('swiftmailer.sender_address') => 'InfinityFXGlobal'))
					->setContentType("text/html")
					->setBody(
						$this->renderView(
							'SiteBundle:Mail:create_account.html.twig',
							array(
								'ip'				=> $request->getClientIp(),
								'account_type'		=> $form->get('accountType')->getData(),
								'date'				=> $form->get('dateSigned')->getData(),
								'name'				=> $form->get('firstName')->getData() . ' ' . $form->get('lastName')->getData(),
								'date_of_birth'		=> $form->get('dateOfBirth')->getData(),
								'address1'			=> $form->get('address1')->getData(),
								'address2'			=> $form->get('address2')->getData(),
								'city'				=> $form->get('city')->getData(),
								'state'				=> $form->get('state')->getData(),
								'postal_code'		=> $form->get('postalCode')->getData(),
								'phone_day'			=> $form->get('phoneDay')->getData(),
								'phone_night'		=> $form->get('phoneNight')->getData(),
								'email'				=> $form->get('email')->getData(),
								'gender'			=> $form->get('gender')->getData(),
								'marital_status'	=> $form->get('maritalStatus')->getData(),
								'dependants'		=> $form->get('dependents')->getData(),
								'citizenship'		=> $form->get('citizenship')->getData(),
								'employment_status'	=> $form->get('employmentStatus')->getData(),
								'employer_name'		=> $form->get('employerName')->getData(),
								'employer_address1'	=> $form->get('employerAddress1')->getData(),
								'employer_address2'	=> $form->get('employerAddress2')->getData(),
								'years_employed'	=> $form->get('yearsEmployed')->getData(),
								'current_position'	=> $form->get('currentPosition')->getData(),
								'phone_business'	=> $form->get('phoneBusiness')->getData(),
								'email_business'	=> $form->get('emailBusiness')->getData(),
								'education_level'	=> $form->get('levelOfEducation')->getData(),
								'annual_income'		=> $form->get('annualIncome')->getData(),
								'liquid_net_worth'	=> $form->get('liquidNetWorth')->getData(),
								'total_net_worth'	=> $form->get('totalNetWorth')->getData(),
								'bank_name'			=> $form->get('bankName')->getData(),
								'bankruptcy'		=> $form->get('bankruptcy')->getData(),
								'bankruptcy_info'	=> $form->get('bankruptcyInfo')->getData(),
								'litigation'		=> $form->get('litigations')->getData(),
								'litigation_info'	=> $form->get('litigationsInfo')->getData(),
								'power_of_attorney'	=> $form->get('powerOfAttourney')->getData(),
								'broker'			=> $form->get('workedForBroker')->getData(),
								'stock'				=> $form->get('stockExperience')->getData(),
								'forex'				=> $form->get('forexExperience')->getData(),
								'commodities'		=> $form->get('commoditiesExperience')->getData(),
								'bonds'				=> $form->get('bondsExperience')->getData(),
								'financial'			=> $form->get('financialInstitutionExperience')->getData(),
								'additional_names'	=> $form->get('additionalNames')->getData(),
								'photo_id'			=> $form->get('govPhotoId')->getData(),
								'utility_bill'		=> $form->get('utilityBill')->getData(),
								'terms_agreement'	=> $form->get('agreeToTerms')->getData(),
							)
						)
					);

				$this->get('mailer')->send($message);

				$sender_confirmation = \Swift_Message::newInstance()
					->setSubject('IFXG - Account Creation Request Confirmation')
					->setTo($form->get('email')->getData())
					->setFrom(array($this->container->getParameter('swiftmailer.sender_address') => 'InfinityFXGlobal'))
					->setContentType("text/html")
					->setBody(
						$this->renderView('SiteBundle:Mail:request_received.html.twig')
					);

				$this->get('mailer')->send($sender_confirmation);

				$request->getSession()->getFlashBag()->add('success', 'Your email has been sent! Thanks!');

				return $this->redirect($this->generateUrl('_create_an_account'));
			}
		}

		return $this->render('SiteBundle:Default:create-an-account.html.twig', array(
			'form' => $form->createView(),
		));

	}

	/**
	 * @return \Symfony\Component\DependencyInjection\ContainerInterface
	 */
	public function getContainer()
	{
		return $this->container;
	}

	public function demoAction()
	{
		return $this->render('SiteBundle:Default:demo.html.twig');
	}

}
