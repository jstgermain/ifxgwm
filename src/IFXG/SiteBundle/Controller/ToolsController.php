<?php

namespace IFXG\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ToolsController extends Controller
{

	public function indexAction()
	{
		return $this->render('SiteBundle:Default:tools.html.twig');
	}

}
