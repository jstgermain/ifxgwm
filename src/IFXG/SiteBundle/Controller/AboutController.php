<?php

namespace IFXG\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AboutController extends Controller
{

	public function indexAction()
	{
		return $this->render('SiteBundle:Default:about.html.twig');
	}

	public function pricingAdvantageAction()
	{
		return $this->render('SiteBundle:Default:pricing-advantage.html.twig');
	}

	public function tradingTechnologyAction()
	{
		return $this->render('SiteBundle:Default:trading-technology.html.twig');
	}

	public function financialSecurityAction()
	{
		return $this->render('SiteBundle:Default:financial-security.html.twig');
	}

	public function forexAction()
	{
		return $this->render('SiteBundle:Default:forex.html.twig');
	}

	public function leverageAction()
	{
		return $this->render('SiteBundle:Default:leverage.html.twig');
	}

	public function makingAndLosingMoneyAction()
	{
		return $this->render('SiteBundle:Default:making-and-losing-money.html.twig');
	}

	public function buyingAndSellingAction()
	{
		return $this->render('SiteBundle:Default:buying-and-selling.html.twig');
	}

	public function theRisksAction()
	{
		return $this->render('SiteBundle:Default:the-risks.html.twig');
	}

	public function currencyQuotesAction()
	{
		return $this->render('SiteBundle:Default:currency-quotes.html.twig');
	}

	public function mostTradableCurrencyPairsAction()
	{
		return $this->render('SiteBundle:Default:most-tradable-currency-pairs.html.twig');
	}

	public function whatMovesACurrencyAction()
	{
		return $this->render('SiteBundle:Default:what-moves-a-currency.html.twig');
	}

	public function hedgingAction()
	{
		return $this->render('SiteBundle:Default:hedging.html.twig');
	}

}
