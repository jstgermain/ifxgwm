<?php

namespace IFXG\SiteBundle\Twig\Extension;

use Symfony\Bundle\FrameworkBundle\Routing\Router;

//<a href="{{ pathOrUrl('') }}"></a>
//<a target="_blank" href="{{ pathOrUrl('') }}"></a>

class PathOrUrlExtension extends \Twig_Extension
{

	private $_router;

	public function __construct(Router $router)
	{
		$this->_router = $router;
	}

	public function getFunctions()
	{
		return array(
			// will call $this->pathOrUrl if pathOrUrl() function is called from twig
			'pathOrUrl' => new \Twig_Function_Method($this, 'pathOrUrl')
		);
	}

	public function pathOrUrl($pathOrUrl)
	{
		// the route collection returns null on undefined routes
		$exists = $this->_router->getRouteCollection()->get($pathOrUrl);
		if (null !== $exists)
		{
			return $this->_router->generate($pathOrUrl);
		}
		return $pathOrUrl;
	}

	public function getName()
	{
		return "pathOrUrl";
	}

}