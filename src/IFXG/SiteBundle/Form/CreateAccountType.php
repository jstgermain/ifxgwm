<?php

namespace IFXG\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CreateAccountType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('accountType', 'hidden', array(
                'label'         => 'Account Type (Individual, Corporate, Managed)',
                'data'          => 'Managed',
            ))
            ->add('dateSigned', 'hidden', array(
                'label'         => 'Date',
                'data'          => date('Y-m-d'),
            ))
            ->add('firstName', 'text', array(
                'label'         => 'First Name'
            ))
            ->add('lastName', 'text', array(
                'label'         => 'Last Name'
            ))
            ->add('dateOfBirth', 'birthday', array(
                'label'         => 'Date of Birth',
	        ))
            ->add('address1', 'text', array(
                'label'         => 'Address'
            ))
            ->add('address2', null, array(
                'label'         => 'Address'
            ))
            ->add('city', 'text', array(
                'label'         => 'City'
            ))
            ->add('state', 'text', array(
                'label'         => 'State'
            ))
            ->add('postalCode', 'text', array(
                'label'         => 'Postal Code'
            ))
            ->add('phoneDay', 'text', array(
                'label'         => 'Telephone, Day'
            ))
            ->add('phoneNight', null, array(
                'label'         => 'Telephone, Evening'
            ))
            ->add('email', 'email', array(
                'label'         => 'Personal E-mail'
            ))
            ->add('gender', 'choice', array(
                'choices'       => array(
                    'Male'          => 'Male',
                    'Female'        => 'Female'
                ),
                'label'         => 'Gender',
                'multiple'      => false,
                'expanded'      => true,
            ))
            ->add('maritalStatus', 'choice', array(
                'choices'       => array(
                    'Single'        => 'Single',
                    'Married'       => 'Married',
                    'Separated'     => 'Separated',
                    'Divorced'      => 'Divorced',
                    'Widowed'       => 'Widowed',
                ),
	            'data'          => 'Single',
                'label'         => 'Marital Status'
            ))
            ->add('dependents', 'choice', array(
                'choices'       => array(
                    '0'             => '0',
                    '1'             => '1',
                    '2'             => '2',
                    '3'             => '3',
                    '4'             => '4',
                    '5'             => '5',
                    '6'             => '6',
                    '7'             => '7',
                    '8'             => '8',
                    '9'             => '9',
                    '10+'            => '10+',
                ),
		        'data'          => '0',
            ))
            ->add('citizenship', 'text', array(
                'label'         => 'Country of Citizenship'
            ))
            ->add('employmentStatus', 'text', array(
                'label'         => 'Employment Status'
            ))
            ->add('employerName', null, array(
                'label'         => 'Employer\'s Name'
            ))
            ->add('employerAddress1', null, array(
                'label'         => 'Employer\'s Address'
            ))
            ->add('employerAddress2', null, array(
                'label'         => 'Employer\'s Address'
            ))
            ->add('yearsEmployed', 'choice', array(
                'choices'       => array(
	                '0-1'           => '0-1',
	                '1-2'           => '1-2',
	                '2-3'           => '2-3',
	                '3-4'           => '3-4',
	                '4-5'           => '4-5',
	                '5-6'           => '5-6',
	                '6-7'           => '6-7',
	                '7-8'           => '7-8',
	                '8-9'           => '8-9',
	                '9-10'          => '9-10',
	                '10+'           => '10+',
                ),
		        'data'          => '0-1',
            ))
            ->add('currentPosition', null, array(
		        'label'         => 'Current Position',
	        ))
            ->add('phoneBusiness', null, array(
		        'label'         => 'Business Phone',
	        ))
            ->add('emailBusiness', null, array(
		        'label'         => 'Business E-mail',
	        ))
            ->add('levelOfEducation', 'choice', array(
                'label'         => 'Level of Education',
                'choices'       => array(
                    'Less Than High School Graduate'        => 'Less Than High School Graduate',
                    'High School Graduate or Equivalent'    => 'High School Graduate or Equivalent',
                    'Some College'                          => 'Some College',
                    'Technical School'                      => 'Technical School',
                    '2-Year College Degree'                 => '2-Year College Degree',
                    'Bachelor\'s Degree'                    => 'Bachelor\'s Degree',
                    'Some Graduate School'                  => 'Some Graduate School',
                    'Master\'s Degree'                      => 'Master\'s Degree',
                    'Doctorate'                             => 'Doctorate',
                ),
		        'data'          => 'Less Than High School Graduate',
            ))
            ->add('annualIncome', 'choice', array(
		        'label'         => 'Annual Income',
		        'choices'       => array(
			        '$0-$25K'       => '$0-$25K',
			        '$25K-$50K'     => '$25K-$50K',
			        '$50K-$100K'    => '$50K-$100K',
			        '$100K-$200K'   => '$100K-$200K',
			        '$200K+'        => '$200K+',
		        ),
	        ))
            ->add('liquidNetWorth', 'choice', array(
		        'label'         => 'Liquid Net Worth',
		        'choices'       => array(
			        '$0-$25K'       => '$0-$25K',
			        '$25K-$50K'     => '$25K-$50K',
			        '$50K-$100K'    => '$50K-$100K',
			        '$100K-$200K'   => '$100K-$200K',
			        '$200K+'        => '$200K+',
		        ),
	        ))
            ->add('totalNetWorth', 'choice', array(
		        'label'         => 'Total Net Worth',
		        'choices'       => array(
			        '$0-$25K'       => '$0-$25K',
			        '$25K-$50K'     => '$25K-$50K',
			        '$50K-$100K'    => '$50K-$100K',
			        '$100K-$200K'   => '$100K-$200K',
			        '$200K+'        => '$200K+',
		        ),
	        ))
            ->add('bankName', 'text', array(
		        'label'         => 'Bank Name',
	        ))
            ->add('bankruptcy', 'choice', array(
                'label'         => 'Ever declared bankruptcy?',
                'choices'       => array(
                    'Yes'       => 'Yes',
                    'No'        => 'No'
                ),
		        'data'          => 'No',
                'multiple'      => false,
                'expanded'      => true,
            ))
            ->add('bankruptcyInfo', null, array(
                'label'         => 'If yes, when?'
            ))
            ->add('litigations', 'choice', array(
                'label'         => 'Any pending litigations?',
                'choices'       => array(
                    'Yes'       => 'Yes',
                    'No'        => 'No'
                ),
		        'data'          => 'No',
                'multiple'      => false,
                'expanded'      => true,
            ))
            ->add('litigationsInfo', null, array(
                'label'         => 'If yes, when?'
            ))
            ->add('powerOfAttourney', 'choice', array(
                'label'         => 'Power of Attorney?',
                'choices'       => array(
                    'Yes'       => 'Yes',
                    'No'        => 'No'
                ),
		        'data'          => 'No',
                'multiple'      => false,
                'expanded'      => true,
            ))
            ->add('workedForBroker', 'choice', array(
                'label'         => 'Have you ever worked for a broker?',
                'choices'       => array(
                    'Yes'       => 'Yes',
                    'No'        => 'No'
                ),
		        'data'          => 'No',
                'multiple'      => false,
                'expanded'      => true,
            ))
            ->add('stockExperience', 'choice', array(
                'label'         => 'Years of stocks experience?',
                'choices'       => array(
                    '0-1'           => '0-1',
                    '1-2'           => '1-2',
                    '2-3'           => '2-3',
                    '3-4'           => '3-4',
                    '4-5'           => '4-5',
                    '5-6'           => '5-6',
                    '6-7'           => '6-7',
                    '7-8'           => '7-8',
                    '8-9'           => '8-9',
                    '9-10'          => '9-10',
                    '10+'           => '10+',
                ),
		        'data'          => '0-1',
            ))
            ->add('forexExperience', 'choice', array(
                'label'         => 'Years of forex experience?',
                'choices'       => array(
	                '0-1'           => '0-1',
	                '1-2'           => '1-2',
	                '2-3'           => '2-3',
	                '3-4'           => '3-4',
	                '4-5'           => '4-5',
	                '5-6'           => '5-6',
	                '6-7'           => '6-7',
	                '7-8'           => '7-8',
	                '8-9'           => '8-9',
	                '9-10'          => '9-10',
	                '10+'           => '10+',
                ),
		        'data'          => '0-1',
            ))
            ->add('commoditiesExperience', 'choice', array(
                'label'         => 'Years of commodities experience?',
                'choices'       => array(
	                '0-1'           => '0-1',
	                '1-2'           => '1-2',
	                '2-3'           => '2-3',
	                '3-4'           => '3-4',
	                '4-5'           => '4-5',
	                '5-6'           => '5-6',
	                '6-7'           => '6-7',
	                '7-8'           => '7-8',
	                '8-9'           => '8-9',
	                '9-10'          => '9-10',
	                '10+'           => '10+',
                ),
		        'data'          => '0-1',
            ))
            ->add('bondsExperience', 'choice', array(
                'label'         => 'Years of bonds experience?',
                'choices'       => array(
	                '0-1'           => '0-1',
	                '1-2'           => '1-2',
	                '2-3'           => '2-3',
	                '3-4'           => '3-4',
	                '4-5'           => '4-5',
	                '5-6'           => '5-6',
	                '6-7'           => '6-7',
	                '7-8'           => '7-8',
	                '8-9'           => '8-9',
	                '9-10'          => '9-10',
	                '10+'           => '10+',
                ),
		        'data'          => '0-1',
            ))
            ->add('financialInstitutionExperience', 'choice', array(
                'label'         => 'Years of stocks experience?',
                'choices'       => array(
	                '0-1'           => '0-1',
	                '1-2'           => '1-2',
	                '2-3'           => '2-3',
	                '3-4'           => '3-4',
	                '4-5'           => '4-5',
	                '5-6'           => '5-6',
	                '6-7'           => '6-7',
	                '7-8'           => '7-8',
	                '8-9'           => '8-9',
	                '9-10'          => '9-10',
	                '10+'           => '10+',
                ),
		        'data'          => '0-1',
            ))
            ->add('additionalNames')
            ->add('govPhotoId', 'choice', array(
                'label'         => 'Government-issued Photo ID?',
                'choices'       => array(
                    'Yes'       => 'Yes',
                    'No'        => 'No'
                ),
		        'data'          => 'Yes',
                'multiple'      => false,
                'expanded'      => true,
            ))
            ->add('utilityBill', 'choice', array(
                'label'         => 'Copy of Utility Bill, with address?',
                'choices'       => array(
                    'Yes'       => 'Yes',
                    'No'        => 'No'
                ),
		        'data'          => 'Yes',
                'multiple'      => false,
                'expanded'      => true,
            ))
            ->add('agreeToTerms', 'choice', array(
                'label'         => 'Do you agree to our Terms and Conditions of Use?',
                'choices'       => array(
                    'Yes'       => 'Yes',
                    'No'        => 'No'
                ),
		        'data'          => 'No',
                'multiple'      => false,
                'expanded'      => true,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IFXG\SiteBundle\Entity\CreateAccount'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ifxg_sitebundle_createaccount';
    }
}
