<?php

namespace IFXG\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'attr' => array(
                    'placeholder'   => 'Enter your full name.',
                    'pattern'       => '.{2,}', //minlength
                )
            ))
            ->add('subject', 'text', array(
                'attr' => array(
                    'placeholder'   => 'What\'s the reason you are contacting us today?',
                    'pattern'       => '.{3,}' //minlength
                )
            ))
            //->add('phoneNumber')
            ->add('email', 'email', array(
                'attr' => array(
                    'placeholder'   => 'Enter your email address.'
                )
            ))
            ->add('message', 'textarea', array(
                'attr' => array(
                    'cols' => 90,
                    'rows' => 10,
                    'placeholder' => 'Please type a message here.'
                )
            ))
            //->add('dateCreated')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IFXG\SiteBundle\Entity\Contact'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ifxg_sitebundle_contact';
    }
}
