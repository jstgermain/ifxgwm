<?php

namespace IFXG\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CreateAccount
 *
 * @ORM\Table(name="create_account")
 * @ORM\Entity(repositoryClass="IFXG\SiteBundle\Entity\CreateAccountRepository")
 * @ORM\HasLifecycleCallbacks
 */
class CreateAccount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="account_type", type="string", length=255)
     */
    private $accountType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_signed", type="string")
     */
    private $dateSigned;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=50)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=50)
     */
    private $lastName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_of_birth", type="date")
     */
    private $dateOfBirth;

    /**
     * @var string
     *
     * @ORM\Column(name="address1", type="string", length=100)
     */
    private $address1;

    /**
     * @var string
     *
     * @ORM\Column(name="address2", nullable=true, type="string", length=100)
     */
    private $address2;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=50)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=50)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=10)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_day", type="string", length=35)
     */
    private $phoneDay;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_night", nullable=true, type="string", length=35)
     */
    private $phoneNight;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=10)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="marital_status", type="string", length=50)
     */
    private $maritalStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="dependents", type="string", length=3)
     */
    private $dependents;

    /**
     * @var string
     *
     * @ORM\Column(name="citizenship", type="string", length=100)
     */
    private $citizenship;

    /**
     * @var string
     *
     * @ORM\Column(name="employment_status", type="string", length=50)
     */
    private $employmentStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="employer_name", nullable=true, type="string", length=50)
     */
    private $employerName;

    /**
     * @var string
     *
     * @ORM\Column(name="employer_address1", nullable=true, type="string", length=100)
     */
    private $employerAddress1;

    /**
     * @var string
     *
     * @ORM\Column(name="employer_address2", nullable=true, type="string", length=100)
     */
    private $employerAddress2;

    /**
     * @var string
     *
     * @ORM\Column(name="years_employed", type="string", length=5)
     */
    private $yearsEmployed;

    /**
     * @var string
     *
     * @ORM\Column(name="current_position", nullable=true, type="string", length=100)
     */
    private $currentPosition;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_business", nullable=true, type="string", length=35)
     */
    private $phoneBusiness;

    /**
     * @var string
     *
     * @ORM\Column(name="email_business", nullable=true, type="string", length=100)
     */
    private $emailBusiness;

    /**
     * @var string
     *
     * @ORM\Column(name="level_of_education", type="string", length=50)
     */
    private $levelOfEducation;

    /**
     * @var string
     *
     * @ORM\Column(name="annual_income", type="string", length=15)
     */
    private $annualIncome;

    /**
     * @var string
     *
     * @ORM\Column(name="liquid_net_worth", type="string", length=15)
     */
    private $liquidNetWorth;

    /**
     * @var string
     *
     * @ORM\Column(name="total_net_worth", type="string", length=15)
     */
    private $totalNetWorth;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_name", type="string", length=100)
     */
    private $bankName;

    /**
     * @var string
     *
     * @ORM\Column(name="bankruptcy", type="string", length=50)
     */
    private $bankruptcy;

    /**
     * @var string
     *
     * @ORM\Column(name="bankruptcy_info", nullable=true, type="text")
     */
    private $bankruptcyInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="litigations", type="string", length=50)
     */
    private $litigations;

    /**
     * @var string
     *
     * @ORM\Column(name="litigations_info", nullable=true, type="text")
     */
    private $litigationsInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="power_of_attourney", type="string", length=50)
     */
    private $powerOfAttourney;

    /**
     * @var string
     *
     * @ORM\Column(name="worked_for_broker", type="string", length=50)
     */
    private $workedForBroker;

    /**
     * @var string
     *
     * @ORM\Column(name="stock_experience", type="string", length=3)
     */
    private $stockExperience;

    /**
     * @var string
     *
     * @ORM\Column(name="forex_experience", type="string", length=3)
     */
    private $forexExperience;

    /**
     * @var string
     *
     * @ORM\Column(name="commodities_experience", type="string", length=3)
     */
    private $commoditiesExperience;

    /**
     * @var string
     *
     * @ORM\Column(name="bonds_experience", type="string", length=3)
     */
    private $bondsExperience;

    /**
     * @var string
     *
     * @ORM\Column(name="financial_institution_experience", type="string", length=3)
     */
    private $financialInstitutionExperience;

    /**
     * @var string
     *
     * @ORM\Column(name="additional_names", nullable=true, type="text")
     */
    private $additionalNames;

    /**
     * @var string
     *
     * @ORM\Column(name="gov_photo_id", type="string", length=20)
     */
    private $govPhotoId;

    /**
     * @var string
     *
     * @ORM\Column(name="utility_bill", type="string", length=20)
     */
    private $utilityBill;

    /**
     * @var string
     *
     * @ORM\Column(name="agree_to_terms", type="string", length=20)
     */
    private $agreeToTerms;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountType
     *
     * @param string $accountType
     * @return CreateAccount
     */
    public function setAccountType($accountType)
    {
        $this->accountType = $accountType;

        return $this;
    }

    /**
     * Get accountType
     *
     * @return string 
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * Set dateSigned
     *
     * @param string $dateSigned
     * @return CreateAccount
     */
    public function setDateSigned($dateSigned)
    {
        $this->dateSigned = $dateSigned;

        return $this;
    }

    /**
     * Get dateSigned
     *
     * @return string
     */
    public function getDateSigned()
    {
        return $this->dateSigned;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return CreateAccount
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return CreateAccount
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     * @return CreateAccount
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return \DateTime 
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return CreateAccount
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string 
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return CreateAccount
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string 
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return CreateAccount
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return CreateAccount
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return CreateAccount
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string 
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set phoneDay
     *
     * @param string $phoneDay
     * @return CreateAccount
     */
    public function setPhoneDay($phoneDay)
    {
        $this->phoneDay = $phoneDay;

        return $this;
    }

    /**
     * Get phoneDay
     *
     * @return string
     */
    public function getPhoneDay()
    {
        return $this->phoneDay;
    }

    /**
     * Set phoneNight
     *
     * @param string $phoneNight
     * @return CreateAccount
     */
    public function setPhoneNight($phoneNight)
    {
        $this->phoneNight = $phoneNight;

        return $this;
    }

    /**
     * Get phoneNight
     *
     * @return string
     */
    public function getPhoneNight()
    {
        return $this->phoneNight;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return CreateAccount
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return CreateAccount
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set maritalStatus
     *
     * @param string $maritalStatus
     * @return CreateAccount
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    /**
     * Get maritalStatus
     *
     * @return string 
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * Set dependents
     *
     * @param string $dependents
     * @return CreateAccount
     */
    public function setDependents($dependents)
    {
        $this->dependents = $dependents;

        return $this;
    }

    /**
     * Get dependents
     *
     * @return string
     */
    public function getDependents()
    {
        return $this->dependents;
    }

    /**
     * Set citizenship
     *
     * @param string $citizenship
     * @return CreateAccount
     */
    public function setCitizenship($citizenship)
    {
        $this->citizenship = $citizenship;

        return $this;
    }

    /**
     * Get citizenship
     *
     * @return string 
     */
    public function getCitizenship()
    {
        return $this->citizenship;
    }

    /**
     * Set employmentStatus
     *
     * @param string $employmentStatus
     * @return CreateAccount
     */
    public function setEmploymentStatus($employmentStatus)
    {
        $this->employmentStatus = $employmentStatus;

        return $this;
    }

    /**
     * Get employmentStatus
     *
     * @return string 
     */
    public function getEmploymentStatus()
    {
        return $this->employmentStatus;
    }

    /**
     * Set employerName
     *
     * @param string $employerName
     * @return CreateAccount
     */
    public function setEmployerName($employerName)
    {
        $this->employerName = $employerName;

        return $this;
    }

    /**
     * Get employerName
     *
     * @return string 
     */
    public function getEmployerName()
    {
        return $this->employerName;
    }

    /**
     * Set employerAddress1
     *
     * @param string $employerAddress1
     * @return CreateAccount
     */
    public function setEmployerAddress1($employerAddress1)
    {
        $this->employerAddress1 = $employerAddress1;

        return $this;
    }

    /**
     * Get employerAddress1
     *
     * @return string 
     */
    public function getEmployerAddress1()
    {
        return $this->employerAddress1;
    }

    /**
     * Set employerAddress2
     *
     * @param string $employerAddress2
     * @return CreateAccount
     */
    public function setEmployerAddress2($employerAddress2)
    {
        $this->employerAddress2 = $employerAddress2;

        return $this;
    }

    /**
     * Get employerAddress2
     *
     * @return string 
     */
    public function getEmployerAddress2()
    {
        return $this->employerAddress2;
    }

    /**
     * Set yearsEmployed
     *
     * @param string $yearsEmployed
     * @return CreateAccount
     */
    public function setYearsEmployed($yearsEmployed)
    {
        $this->yearsEmployed = $yearsEmployed;

        return $this;
    }

    /**
     * Get yearsEmployed
     *
     * @return string
     */
    public function getYearsEmployed()
    {
        return $this->yearsEmployed;
    }

    /**
     * Set currentPosition
     *
     * @param string $currentPosition
     * @return CreateAccount
     */
    public function setCurrentPosition($currentPosition)
    {
        $this->currentPosition = $currentPosition;

        return $this;
    }

    /**
     * Get currentPosition
     *
     * @return string 
     */
    public function getCurrentPosition()
    {
        return $this->currentPosition;
    }

    /**
     * Set phoneBusiness
     *
     * @param string $phoneBusiness
     * @return CreateAccount
     */
    public function setPhoneBusiness($phoneBusiness)
    {
        $this->phoneBusiness = $phoneBusiness;

        return $this;
    }

    /**
     * Get phoneBusiness
     *
     * @return string
     */
    public function getPhoneBusiness()
    {
        return $this->phoneBusiness;
    }

    /**
     * Set emailBusiness
     *
     * @param string $emailBusiness
     * @return CreateAccount
     */
    public function setEmailBusiness($emailBusiness)
    {
        $this->emailBusiness = $emailBusiness;

        return $this;
    }

    /**
     * Get emailBusiness
     *
     * @return string 
     */
    public function getEmailBusiness()
    {
        return $this->emailBusiness;
    }

    /**
     * Set levelOfEducation
     *
     * @param string $levelOfEducation
     * @return CreateAccount
     */
    public function setLevelOfEducation($levelOfEducation)
    {
        $this->levelOfEducation = $levelOfEducation;

        return $this;
    }

    /**
     * Get levelOfEducation
     *
     * @return string 
     */
    public function getLevelOfEducation()
    {
        return $this->levelOfEducation;
    }

    /**
     * Set annualIncome
     *
     * @param string $annualIncome
     * @return CreateAccount
     */
    public function setAnnualIncome($annualIncome)
    {
        $this->annualIncome = $annualIncome;

        return $this;
    }

    /**
     * Get annualIncome
     *
     * @return string
     */
    public function getAnnualIncome()
    {
        return $this->annualIncome;
    }

    /**
     * Set liquidNetWorth
     *
     * @param string $liquidNetWorth
     * @return CreateAccount
     */
    public function setLiquidNetWorth($liquidNetWorth)
    {
        $this->liquidNetWorth = $liquidNetWorth;

        return $this;
    }

    /**
     * Get liquidNetWorth
     *
     * @return string
     */
    public function getLiquidNetWorth()
    {
        return $this->liquidNetWorth;
    }

    /**
     * Set totalNetWorth
     *
     * @param string $totalNetWorth
     * @return CreateAccount
     */
    public function setTotalNetWorth($totalNetWorth)
    {
        $this->totalNetWorth = $totalNetWorth;

        return $this;
    }

    /**
     * Get totalNetWorth
     *
     * @return string
     */
    public function getTotalNetWorth()
    {
        return $this->totalNetWorth;
    }

    /**
     * Set bankName
     *
     * @param string $bankName
     * @return CreateAccount
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;

        return $this;
    }

    /**
     * Get bankName
     *
     * @return string 
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * Set bankruptcy
     *
     * @param string $bankruptcy
     * @return CreateAccount
     */
    public function setBankruptcy($bankruptcy)
    {
        $this->bankruptcy = $bankruptcy;

        return $this;
    }

    /**
     * Get bankruptcy
     *
     * @return integer 
     */
    public function getBankruptcy()
    {
        return $this->bankruptcy;
    }

    /**
     * Set bankruptcyInfo
     *
     * @param string $bankruptcyInfo
     * @return CreateAccount
     */
    public function setBankruptcyInfo($bankruptcyInfo)
    {
        $this->bankruptcyInfo = $bankruptcyInfo;

        return $this;
    }

    /**
     * Get bankruptcyInfo
     *
     * @return string 
     */
    public function getBankruptcyInfo()
    {
        return $this->bankruptcyInfo;
    }

    /**
     * Set litigations
     *
     * @param string $litigations
     * @return CreateAccount
     */
    public function setLitigations($litigations)
    {
        $this->litigations = $litigations;

        return $this;
    }

    /**
     * Get litigations
     *
     * @return string
     */
    public function getLitigations()
    {
        return $this->litigations;
    }

    /**
     * Set litigationsInfo
     *
     * @param string $litigationsInfo
     * @return CreateAccount
     */
    public function setLitigationsInfo($litigationsInfo)
    {
        $this->litigationsInfo = $litigationsInfo;

        return $this;
    }

    /**
     * Get litigationsInfo
     *
     * @return string 
     */
    public function getLitigationsInfo()
    {
        return $this->litigationsInfo;
    }

    /**
     * Set powerOfAttourney
     *
     * @param string $powerOfAttourney
     * @return CreateAccount
     */
    public function setPowerOfAttourney($powerOfAttourney)
    {
        $this->powerOfAttourney = $powerOfAttourney;

        return $this;
    }

    /**
     * Get powerOfAttourney
     *
     * @return string 
     */
    public function getPowerOfAttourney()
    {
        return $this->powerOfAttourney;
    }

    /**
     * Set workedForBroker
     *
     * @param string $workedForBroker
     * @return CreateAccount
     */
    public function setWorkedForBroker($workedForBroker)
    {
        $this->workedForBroker = $workedForBroker;

        return $this;
    }

    /**
     * Get workedForBroker
     *
     * @return string 
     */
    public function getWorkedForBroker()
    {
        return $this->workedForBroker;
    }

    /**
     * Set stockExperience
     *
     * @param string $stockExperience
     * @return CreateAccount
     */
    public function setStockExperience($stockExperience)
    {
        $this->stockExperience = $stockExperience;

        return $this;
    }

    /**
     * Get stockExperience
     *
     * @return string
     */
    public function getStockExperience()
    {
        return $this->stockExperience;
    }

    /**
     * Set forexExperience
     *
     * @param string $forexExperience
     * @return CreateAccount
     */
    public function setForexExperience($forexExperience)
    {
        $this->forexExperience = $forexExperience;

        return $this;
    }

    /**
     * Get forexExperience
     *
     * @return string
     */
    public function getForexExperience()
    {
        return $this->forexExperience;
    }

    /**
     * Set commoditiesExperience
     *
     * @param string $commoditiesExperience
     * @return CreateAccount
     */
    public function setCommoditiesExperience($commoditiesExperience)
    {
        $this->commoditiesExperience = $commoditiesExperience;

        return $this;
    }

    /**
     * Get commoditiesExperience
     *
     * @return string
     */
    public function getCommoditiesExperience()
    {
        return $this->commoditiesExperience;
    }

    /**
     * Set bondsExperience
     *
     * @param string $bondsExperience
     * @return CreateAccount
     */
    public function setBondsExperience($bondsExperience)
    {
        $this->bondsExperience = $bondsExperience;

        return $this;
    }

    /**
     * Get bondsExperience
     *
     * @return string
     */
    public function getBondsExperience()
    {
        return $this->bondsExperience;
    }

    /**
     * Set financialInstitutionExperience
     *
     * @param string $financialInstitutionExperience
     * @return CreateAccount
     */
    public function setFinancialInstitutionExperience($financialInstitutionExperience)
    {
        $this->financialInstitutionExperience = $financialInstitutionExperience;

        return $this;
    }

    /**
     * Get financialInstitutionExperience
     *
     * @return string
     */
    public function getFinancialInstitutionExperience()
    {
        return $this->financialInstitutionExperience;
    }

    /**
     * Set additionalNames
     *
     * @param string $additionalNames
     * @return CreateAccount
     */
    public function setAdditionalNames($additionalNames)
    {
        $this->additionalNames = $additionalNames;

        return $this;
    }

    /**
     * Get additionalNames
     *
     * @return string 
     */
    public function getAdditionalNames()
    {
        return $this->additionalNames;
    }

    /**
     * Set govPhotoId
     *
     * @param string $govPhotoId
     * @return CreateAccount
     */
    public function setGovPhotoId($govPhotoId)
    {
        $this->govPhotoId = $govPhotoId;

        return $this;
    }

    /**
     * Get govPhotoId
     *
     * @return string
     */
    public function getGovPhotoId()
    {
        return $this->govPhotoId;
    }

    /**
     * Set utilityBill
     *
     * @param string $utilityBill
     * @return CreateAccount
     */
    public function setUtilityBill($utilityBill)
    {
        $this->utilityBill = $utilityBill;

        return $this;
    }

    /**
     * Get utilityBill
     *
     * @return string 
     */
    public function getUtilityBill()
    {
        return $this->utilityBill;
    }

    /**
     * Set agreeToTerms
     *
     * @param string $agreeToTerms
     * @return CreateAccount
     */
    public function setAgreeToTerms($agreeToTerms)
    {
        $this->agreeToTerms = $agreeToTerms;

        return $this;
    }

    /**
     * Get agreeToTerms
     *
     * @return string 
     */
    public function getAgreeToTerms()
    {
        return $this->agreeToTerms;
    }

}
