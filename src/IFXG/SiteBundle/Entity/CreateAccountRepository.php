<?php

namespace IFXG\SiteBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * CreateAccountRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CreateAccountRepository extends EntityRepository
{
}
