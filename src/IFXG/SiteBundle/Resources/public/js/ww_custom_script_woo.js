(function ($) {
	"use strict";

	$('.ww-select-wrapper').each(function () {
		$(this).prepend('<span>' + $(this).find('.ww-select option:selected').text() + '</span>');
	});

})(jQuery);