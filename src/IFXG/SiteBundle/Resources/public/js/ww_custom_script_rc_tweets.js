(function ($) {
	"use strict";
	jQuery(document).ready(function(){
		var slider = jQuery('#ww-latest-twitter'+ww_rc_tweets_id_widget.id_widget+' .tp_recent_tweets').bxSlider({
			mode: 'vertical',
			slideMargin: 5,
			auto: true,
			moveSlides:1,
			minSlides: ww_rc_tweets_instance.tweetstoshow,
			autoHover:1,
			touchEnabled:false,
			speed:4000,
			pause:4000,
			pager: false,
			controls: false
		});
	});
})(jQuery);